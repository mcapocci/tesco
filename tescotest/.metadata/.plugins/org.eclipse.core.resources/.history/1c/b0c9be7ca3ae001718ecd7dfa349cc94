package uk.me.capocci.tescotest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Encapsulates the state of a vending machine and the operations that can be performed on it
 */
public class VendingMachineImpl implements VendingMachine {
	
	private boolean on = false;
	private final Map<Coin, Integer> machineFloat = new HashMap<>();
	private final List<Coin> heldCoins = new ArrayList<>();
	private final Map<Item, Integer> stock = new HashMap<>(3);
	private String message;
	
	private VendingMachineImpl() {
		super();
		
	}
	
	public static VendingMachine exampleVendingMachine()
	{
		VendingMachineImpl machine = new VendingMachineImpl();
		machine.createDefaultStock();
		machine.createDefaultFloat();
		return machine;
	}
	
	private void createDefaultStock()
	{
		Item.getItem("A").ifPresent(i -> stock.put(i, 5));
		Item.getItem("B").ifPresent(i -> stock.put(i, 2));
		Item.getItem("C").ifPresent(i -> stock.put(i, 7));
	}
	private void createDefaultFloat()
	{
		machineFloat.put(Coin.identifyCoin(10), 1);
		machineFloat.put(Coin.identifyCoin(20), 3);
		machineFloat.put(Coin.identifyCoin(50), 3);
		machineFloat.put(Coin.identifyCoin(100), 7);
	}
	
	/*
	 * The machine vends items, holds change and displays a message. On certain interactions, it assumes a new transaction is happening.
	 * If it is holding money, the next customer is lucky!
	 */
	private void clearState()
	{
		setMessage("");
	}
	
	private void setMessage(String message)
	{
		this.message = message;
	}
	
	@Override
	public boolean isOn() {
		return on;
	}
	
	@Override
	public void turnOn() 
	{
		if (!on)
		{
			on = true;
			clearState();
		}
	}
	
	@Override
	public void turnOff() {
		on = false;
	}

	@Override
	public Optional<Coin> insertCoin(Coin coin)
	{
		if (coin.isAccepted())
		{
			getHeldCoins().add(coin);
			addCoinToFloat(coin);
			return Optional.empty();
		}
		else
			return Optional.of(coin);
	}
	
	public List<Coin> getHeldCoins() {
		return heldCoins;
	}
	
	private int heldSum()
	{
		return getHeldCoins().stream().collect(Collectors.summingInt(c -> c.getQuantity()));
	}
	
	private void addCoinToFloat(Coin coin)
	{
		machineFloat.compute(coin, (c,n) -> n + 1);
	}
		
	private void deliverItem(Item item)
	{
		stock.compute(item, (i, n) -> n - 1);
	}

	@Override
	public List<Coin> returnCoins() 
	{
		List<Coin> toReturn = new ArrayList<>(getHeldCoins());
		getHeldCoins().clear();
		clearState();
		return toReturn;
	}
	
	/*
	 * The machines buttons are hard-wired to the vending trays, so {@code name} will always match a product.
	 * @see uk.me.capocci.tescotest.VendingMachine#vendItem(java.lang.String)
	 */
	@Override
	public Optional<Item> vendItem(String name) 
	{
		Item item = Item.getItem(name).orElseThrow(() -> new RuntimeException());
		int change = heldSum() - item.getPrice();

		if (change >= 0)
		{
			if(changeIsAvailable(change))
			{
				deliverItem(item);
				getHeldCoins().clear();
				setMessage("Thank you");
				return Optional.of(item);
			}
			else
			{
				setMessage("Exact Change Required");
				return Optional.empty();
			}
		}
		else
		{
			setMessage("Insufficient Money Inserted");
			return Optional.empty();
		}
	}

	/*
	 * When the customer requests a product, and change is required, check I actually have it. If I do, it is safe to allow the vend to continue.
	 * The customer may then request a coin return, or add more money to make a second purchase.
	 */
	private boolean changeIsAvailable(int change)
	{
		return change == 0;
	}
	
	@Override
	public String getMessage() 
	{
		return message;
	}

	@Override
	public String getCashInsertedMessage() 
	{
		return Integer.toString(heldSum()) + "p";
	}
	
	
	
}
