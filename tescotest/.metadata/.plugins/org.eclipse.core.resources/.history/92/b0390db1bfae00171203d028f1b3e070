package uk.me.capocci.tescotest;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class VendingSteps {

	private VendingMachine machine = VendingMachine.exampleVendingMachine();
	private List<CoinImpl> latestReturn;
	private CoinImpl lastInserted;
	private Optional<Item> itemReceived;
	
	public VendingSteps()
	{
		
	}
	
	@Given("^The vending machine is (on|off)$")
	@When("^I switch (on|off) the machine$")
	public void togglePower(String action) 
	{
		if (action.equals("on"))
			machine.turnOn();
		else
			machine.turnOff();
	}
	
	@Then("^The machine is (on|off)$")
	public void checkMachine(String state)
	{
		assertEquals(state.equals("on"), machine.isOn());
	}
	
	@Given("^I inserted ([1-9]\\d*|0)(p|c)$")
	@When("^I insert ([1-9]\\d*)(p|c)$")
	public void addCoin(int quantity, char denomination)
	{
		if(quantity != 0)
		{	
			lastInserted = denomination == 'p' ? CoinImpl.identifyCoin(quantity) : CoinImpl.UNKNOWN;
			Optional<CoinImpl> returnCoin = machine.insertCoin(lastInserted);
			returnCoin.ifPresent(c -> latestReturn = Collections.singletonList(c));
		}
	}
	
	@When("^I request coin return")
	public void coinReturn()
	{
		latestReturn = machine.returnCoins();
	}
	
	@Given("^Machine has ([1-9]) ([1-9]\\d*)p coin$")
	public void setAvailableCoins(int number, int quantity)
	{
		CoinImpl coin = CoinImpl.identifyCoin(quantity);
		machine.setFloatLevel(coin, number);
	}
	
	@Given("^product (.*) is unavailable$")
	public void setProductUnavailable(String name)
	{
		machine.setProductLevel(name, 0);
	}
	
	@When("^I select (.*)$")
	public void selectProduct(String name)
	{
		itemReceived = machine.vendItem(name);
	}
	
	@Then("^the machine displays \"([^\"]*)\"$")
	public void checkDisplay(String message)
	{
		assertEquals("Machine displaying wrong message", message, machine.getMessage());
	}
	
	@Then("^the machine displays (\\d+p)$")
	public void checkMoneyDisplay(String amount)
	{
		assertEquals("Machine displaying wrong amount",amount, machine.getCustomerCashMessage());
	}
		
	@Then("^I receive ([1-9]\\d*)p$")
	public void receiveCoins(int quantity)
	{
		assertNotNull("No coins returned", latestReturn);
		int returnedSum = latestReturn.stream().collect(Collectors.summingInt(c -> c.getQuantity()));
		assertEquals("Coins of wrong value returned", quantity, returnedSum);
	}
	
	@Then("^I receive no change$")
	public void receiveNoCoins()
	{
		assertNull("Coins return when none expected", latestReturn);
	}
	
	@Then("^I receive coin back$")
	public void receiveCoinBack()
	{
		assertNotNull("Unaccepted coin not returned", latestReturn);
		assertEquals("More than just the Unaccepted coin returned", 1, latestReturn.size());
		assertEquals("Wrong coin returned", latestReturn.get(0), lastInserted);
	}
	
	@Then("^product (.*) is released$")
	public void productReceived(String name)
	{
		assertTrue("Item not delivered", itemReceived.isPresent());
		assertEquals("Wrong item delivered", itemReceived.get().getName(), name);
	}
	@Then("^no product is released$")
	public void productNotReceived()
	{
		assertFalse("Item erroneously delivered", itemReceived.isPresent());
	}
}
