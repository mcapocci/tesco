package uk.me.capocci.tescotest;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class VendingSteps {

	private VendingMachine machine = VendingMachineImpl.exampleVendingMachine();
	private List<Coin> latestReturn;
	private Coin lastInserted;
	private Optional<Item> itemReceived;
	
	@Given("^The vending machine is (on|off)$")
	@When("^I switch (on|off) the machine$")
	public void togglePower(String action) 
	{
		if (action.equals("on"))
			machine.turnOn();
		else
			machine.turnOff();
	}
	
	@Then("^The machine is (on|off)$")
	public void checkMachine(String state)
	{
		assertEquals(state.equals("on"), machine.isOn());
	}
	
	@Given("^I inserted ([1-9]\\d*)(p|c)$")
	@When("^I insert ([1-9]\\d*)(p|c)$")
	public void addCoin(int quantity, char denomination)
	{
		lastInserted = denomination == 'p' ? Coin.identifyCoin(quantity) : Coin.UNKNOWN;
		Optional<Coin> returnCoin = machine.insertCoin(lastInserted);
		returnCoin.ifPresent(c -> latestReturn = Collections.singletonList(c));
	}
	
	@When("^I request coin return")
	public void coinReturn()
	{
		latestReturn = machine.returnCoins();
	}
	@When("^I select (.*)$")
	public void selectProduct(String name)
	{
		itemReceived = machine.vendItem(name);
	}
	
	@Then("^the machine displays ([a-zA-Z]+)$")
	public void checkDisplay(String message)
	{
		assertEquals(message, machine.getMessage());
	}
	
	@Then("^the machine displays (\\d+)p$")
	public void checkMoneyDisplay(int amount)
	{
		assertEquals(amount, machine.getCashInserted());
	}
		
	@Then("^I receive ([1-9]\\d*|0)p$")
	public void receiveCoins(int quantity)
	{
		if(quantity == 0)
		{
			assertNull(latestReturn);
		}
		else
		{
			assertNotNull(latestReturn);
			int returnedSum = latestReturn.stream().collect(Collectors.summingInt(c -> c.getQuantity()));
			assertEquals(quantity, returnedSum);
		}
	}
	
	@Then("^I receive coin back$")
	public void receiveCoinBack()
	{
		assertNotNull(latestReturn);
		assertEquals(1, latestReturn.size());
		assertEquals(latestReturn.get(0), lastInserted);
	}
	
	@Then("^product (.*) is released$")
	public void productReceived(String name)
	{
		assertTrue(itemReceived.isPresent());
		assertEquals(itemReceived.get().getName(), name);
	}
	@Then("^no product is released$")
	public void productNotReceived()
	{
		assertFalse(itemReceived.isPresent());
	}
}
