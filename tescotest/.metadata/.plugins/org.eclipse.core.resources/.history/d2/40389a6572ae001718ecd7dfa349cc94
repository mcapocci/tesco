package uk.me.capocci.tescotest;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Coin {

	private static Set<Coin> ACCEPTED_COINS = new HashSet<>();
	private static Map<Integer, Coin> KNOWN_COINS = new HashMap<>();
	
	static
	{
		ACCEPTED_COINS.add(new Coin(10));
		ACCEPTED_COINS.add(new Coin(20));
		ACCEPTED_COINS.add(new Coin(50));
		ACCEPTED_COINS.add(new Coin(100));
		
		ACCEPTED_COINS.forEach(c -> KNOWN_COINS.put(c.getPence(), c));
		KNOWN_COINS.put(1, new Coin(1));
		KNOWN_COINS.put(2, new Coin(2));
		KNOWN_COINS.put(5, new Coin(5));
		KNOWN_COINS.put(200, new Coin(200));
	}
	
	public static Coin identifyCoin(int pence)
	{
		return KNOWN_COINS.getOrDefault(pence, new Coin(11));
	}
	
	private final int pence;
	
	private Coin(int pence)
	{
		this.pence = pence;
	}

	public int getPence() {
		return pence;
	}
	
	public boolean isAccepted()
	{
		return getPence() != 11;
	}
	
	
}
