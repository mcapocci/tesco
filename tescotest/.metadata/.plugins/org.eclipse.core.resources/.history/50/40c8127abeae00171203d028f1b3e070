package uk.me.capocci.tescotest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import uk.me.capocci.tescotest.exceptions.InvalidProductException;

/**
 * Encapsulates the state of a vending machine and the operations that can be performed on it.
 * Coins may be entered in certain denominations, other coins are ejected immediately.
 * Any balance may be returned to the customer.
 * After a successful vend, the customer may make a further purchase, adding money if necessary, or request his change returned.
 * If the machine is not certain it will have change for a given vend, it will refuse.
 */
public class VendingMachineImpl implements VendingMachine {
	
	private final Map<CoinImpl, Integer> machineFloat = new HashMap<>();
	private final List<CoinImpl> heldCoins = new ArrayList<>();
	private final Map<Item, Integer> stock = new HashMap<>(3);
	private String message;
	
	protected VendingMachineImpl() {
		super();
		
	}
		
	void createDefaultStock()
	{
		Item.getItem("A").ifPresent(i -> stock.put(i, 5));
		Item.getItem("B").ifPresent(i -> stock.put(i, 2));
		Item.getItem("C").ifPresent(i -> stock.put(i, 7));
	}
	void createDefaultFloat()
	{
		machineFloat.put(CoinImpl.identifyCoin(10), 1);
		machineFloat.put(CoinImpl.identifyCoin(20), 3);
		machineFloat.put(CoinImpl.identifyCoin(50), 3);
		machineFloat.put(CoinImpl.identifyCoin(100), 7);
	}
	
	private void clearMessage()
	{
		setMessage("");
	}
	
	private void setMessage(String message)
	{
		this.message = message;
	}
	
	@Override
	public boolean isOn() {
		return true;
	}
	
	@Override
	public void turnOn() 
	{
		clearMessage();
	}
	
	@Override
	public void turnOff() {
		
	}

	@Override
	public Optional<CoinImpl> insertCoin(CoinImpl coin)
	{
		if (coin.isAccepted())
		{
			getHeldCoins().add(coin);
			addCoinToFloat(coin);
			return Optional.empty();
		}
		else
			return Optional.of(coin);
	}
	
	public List<CoinImpl> getHeldCoins() {
		return heldCoins;
	}
	
	private int heldSum()
	{
		return getHeldCoins().stream().collect(Collectors.summingInt(c -> c.getDenomination()));
	}
	
	private void addCoinToFloat(CoinImpl coin)
	{
		getMachineFloat().compute(coin, (c,n) -> n + 1);
	}
		
	private void deliverItem(Item item)
	{
		getStock().compute(item, (i, n) -> n - 1);
	}

	@Override
	public List<CoinImpl> returnCoins() 
	{
		List<CoinImpl> toReturn = new ArrayList<>(getHeldCoins());
		getHeldCoins().clear();
		clearMessage();
		return toReturn;
	}
	
	private boolean hasItem(Item item)
	{
		return getStock().getOrDefault(item, 0) > 0;
	}
	
	/*
	 * The machines buttons are hard-wired to the vending trays, so {@code name} will always match a product.
	 * @see uk.me.capocci.tescotest.VendingMachine#vendItem(java.lang.String)
	 */
	@Override
	public Optional<Item> vendItem(String name) throws InvalidProductException
	{
		Item item = Item.getItem(name).orElseThrow(() -> new InvalidProductException());
		if (!hasItem(item))
		{
			setMessage("Item Not Available");
			return Optional.empty();
		}
		
		int change = heldSum() - item.getPrice();

		if (change >= 0)
		{
			Optional<List<CoinImpl>> changeCoins = calculateChangeCoins(change);
			if(changeCoins.isPresent())
			{
				deliverItem(item);
				getHeldCoins().clear();
				getHeldCoins().addAll(changeCoins.get());
				setMessage("Thank you");
				return Optional.of(item);
			}
			else
			{
				setMessage("Exact Change Required");
				return Optional.empty();
			}
		}
		else
		{
			setMessage("Insufficient Money Inserted");
			return Optional.empty();
		}
	}

	/*
	 * When the customer requests a product, and change is required, check I actually have it. If I do, it is safe to allow the vend to continue.
	 * The customer may then request a coin return, or add more money to make a second purchase.
	 */
	private Optional<List<CoinImpl>> calculateChangeCoins(int changeRequired)
	{	
		Map<CoinImpl, Integer> changeAvailable = new HashMap<>(getMachineFloat());
		List<CoinImpl> changeCoins = new ArrayList<>();
		
		while(changeRequired > 0)
		{
			final int changeRequiredNow = changeRequired;
			
			// Find the biggest coin we have which is less than or equal to the change required
			Optional<CoinImpl> changeCoin = 
					changeAvailable.entrySet().stream().filter(e -> e.getValue() > 0).map(e -> e.getKey())
					.sorted().filter(c -> c.getDenomination() <= changeRequiredNow).findFirst();
		
			if (changeCoin.isPresent())
			{
				CoinImpl coin = changeCoin.get();
				changeRequired -= coin.getDenomination();
				changeCoins.add(coin);
				changeAvailable.compute(coin, (c, n) -> n - 1);
			}
			else
				return Optional.empty();
		}	
		return Optional.of(changeCoins);
	}
		
	@Override
	public String getMessage() 
	{
		return message;
	}

	@Override
	public String getCustomerCashMessage() 
	{
		return Integer.toString(heldSum()) + "p";
	}

	private Map<CoinImpl, Integer> getMachineFloat() {
		return machineFloat;
	}

	public Map<Item, Integer> getStock() {
		return stock;
	}
	
	@Override
	public void setFloatLevel(CoinImpl coin, int number) throws IllegalArgumentException
	{
		if (coin == null || number < 0)
			throw new IllegalArgumentException("Coin is null or number is negative");
		getMachineFloat().put(coin, number);
	}
	
	@Override
	public void setProductLevel(String name, int number) throws IllegalArgumentException
	{
		if (name == null || number < 0)
			throw new IllegalArgumentException("Name is null or number is negative");
		Item item = Item.getItem(name).orElseThrow(() -> new InvalidProductException());
		getStock().put(item, number);
	}
	
}
