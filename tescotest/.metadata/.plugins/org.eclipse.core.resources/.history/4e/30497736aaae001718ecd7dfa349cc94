package uk.me.capocci.tescotest;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Coin implements Comparable<Coin> {

	private static final Set<Coin> ACCEPTED_COINS = new HashSet<>();
	private static final Map<Integer, Coin> KNOWN_COINS = new HashMap<>();
	public static final Coin UNKNOWN = new Coin(-1);
	
	static
	{
		ACCEPTED_COINS.add(new Coin(10));
		ACCEPTED_COINS.add(new Coin(20));
		ACCEPTED_COINS.add(new Coin(50));
		ACCEPTED_COINS.add(new Coin(100));
		
		ACCEPTED_COINS.forEach(c -> KNOWN_COINS.put(c.getQuantity(), c));
		KNOWN_COINS.put(1, new Coin(1));
		KNOWN_COINS.put(2, new Coin(2));
		KNOWN_COINS.put(5, new Coin(5));
		KNOWN_COINS.put(200, new Coin(200));
	}
		
	public static Coin identifyCoin(int quantity)
	{
		return KNOWN_COINS.getOrDefault(quantity, UNKNOWN);
	}
		
	private final int quantity;
	
	private Coin(int quantity)
	{
		this.quantity = quantity;
	}
	
	public boolean isAccepted()
	{
		return ACCEPTED_COINS.contains(this);
	}

	public int getQuantity() {
		return quantity;
	}

	@Override
	public int compareTo(Coin o) 
	{
		return Integer.signum(getQuantity() - o.getQuantity());
	}

}
