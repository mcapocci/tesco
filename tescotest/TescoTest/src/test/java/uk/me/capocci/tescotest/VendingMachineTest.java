package uk.me.capocci.tescotest;

import static org.junit.Assert.*;
import org.junit.Test;


/**
 * Unit tests for {@link VendingMachineImpl}
 */
public class VendingMachineTest {
	
	@Test
	public void defaultStateIsOff() {
		VendingMachine machine = VendingMachine.exampleVendingMachine();
		assertFalse(machine.isOn());
	}
	

}
