package uk.me.capocci.tescotest.lambda;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import uk.me.capocci.tescotest.Coin;
import uk.me.capocci.tescotest.CoinImpl;
import uk.me.capocci.tescotest.Item;
import uk.me.capocci.tescotest.VendingMachine;
import cucumber.api.java8.En;

public class VendingSteps implements En {

	private VendingMachine machine = VendingMachine.exampleVendingMachine();
	private List<Coin> latestReturn;
	private Coin lastInserted;
	private Optional<Item> itemReceived;
	
	
	public VendingSteps() {
		super();

		Given("^The vending machine is (on|off)$", (String s) -> togglePower(s));
		Given("^Machine has ([1-9]) ([1-9]\\d*)p coin$", (Integer n, Integer q) -> machine.setFloatLevel(CoinImpl.identifyCoin(q), n));
		Given("^product (.*) is unavailable$", (String s) -> machine.setProductLevel(s, 0));
		Given("^I inserted ([1-9]\\d*|0)(p|c)$", (Integer n, Character d) -> addCoin(n,d));
		
		When("^I switch (on|off) the machine$", (String s) -> togglePower(s));
		When("^I request coin return$", () -> latestReturn = machine.returnCoins());
		When("^I select (.*)$", (String s) -> itemReceived = machine.vendItem(s));
		When("^I insert ([1-9]\\d*)(p|c)$", (Integer n, Character d) -> addCoin(n,d));
		
		Then("^The machine is (on|off)$", (String s) -> assertEquals(s.equals("on"), machine.isOn()));
		Then("^the machine displays \"([^\"]*)\"$", (String s) -> assertEquals("Machine displaying wrong message", s, machine.getMessage()));
		Then("^the machine displays (\\d+p)$", (String s) -> assertEquals("Machine displaying wrong amount",s , machine.getCustomerCashMessage()));
		Then("^I receive ([1-9]\\d*)p$", (Integer q) -> {
			assertNotNull("No coins returned", latestReturn);
			int returnedSum = latestReturn.stream().collect(Collectors.summingInt(c -> c.getDenomination()));
			assertEquals("Coins of wrong value returned", q.intValue(), returnedSum);
		});
		Then("^I receive no change$", () -> assertEquals("Coins return when none expected", 0, latestReturn.size()));
		Then("^I receive coin back$", () -> {
			assertNotNull("Unaccepted coin not returned", latestReturn);
			assertEquals("More than just the Unaccepted coin returned", 1, latestReturn.size());
			assertEquals("Wrong coin returned", latestReturn.get(0), lastInserted);
		});
		Then("^no product is released$", () -> assertFalse("Item erroneously delivered", itemReceived.isPresent()));
		Then("^product (.*) is released$", (String s) -> {
			assertTrue("Item not delivered", itemReceived.isPresent());
			assertEquals("Wrong item delivered", itemReceived.get().getName(), s);
		});
	}

	
	public void togglePower(String action) 
	{
		if (action.equals("on"))
			machine.turnOn();
		else
			machine.turnOff();
	}
	
	public void addCoin(int quantity, char denomination)
	{
		if(quantity != 0)
		{	
			lastInserted = denomination == 'p' ? CoinImpl.identifyCoin(quantity) : CoinImpl.UNKNOWN;
			Optional<Coin> returnCoin = machine.insertCoin(lastInserted);
			returnCoin.ifPresent(c -> latestReturn = Collections.singletonList(c));
		}
	}

}
