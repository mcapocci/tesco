package uk.me.capocci.tescotest;

public class UnknownCoin implements Coin {

	/**
	 * An unknown coin has no known denomination
	 */
	@Override
	public int getDenomination() {
		throw new UnsupportedOperationException("An unknown coin's denomination is uncertain");
	}

}
