package uk.me.capocci.tescotest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/*
 * No point cluttering the actual {@code VendingMachineImpl} with on/off logic, so wrap with this.
 * Don't throw an exception, when a machine is off, it just doesn't respond!
 */
public class VendingMachineSwitch implements VendingMachine 
{
	private final VendingMachine wrapped;
	private boolean on = false;
	
	public VendingMachineSwitch(VendingMachine wrapped)
	{
		this.wrapped = wrapped;
	}

	@Override
	public boolean isOn() {
		return on;
	}
	
	@Override
	public void turnOn() 
	{
		if (!on)
		{
			on = true;
			wrapped.turnOn();
		}
	}
	
	@Override
	public void turnOff() {
		if(on)
		{
			on = false;
			wrapped.turnOff();
		}
	}

	@Override
	public String getMessage() {
		return on ? wrapped.getMessage() : "";
	}

	@Override
	public String getCustomerCashMessage() {
		return on ? wrapped.getCustomerCashMessage() : "";
	}

	@Override
	public Optional<Coin> insertCoin(Coin coin) {
		
		return on ? wrapped.insertCoin(coin) : Optional.empty();
	}

	@Override
	public List<Coin> returnCoins() {

		return on ? wrapped.returnCoins() : new ArrayList<>();
	}

	@Override
	public Optional<Item> vendItem(String name) {
		
		return on ? wrapped.vendItem(name) : Optional.empty();
	}

	@Override
	public void setFloatLevel(Coin coin, int number) throws IllegalArgumentException {
		if (on)
			wrapped.setFloatLevel(coin, number);
	}

	@Override
	public void setProductLevel(String name, int number) throws IllegalArgumentException {
		if (on)
			wrapped.setProductLevel(name, number);
	}
}
