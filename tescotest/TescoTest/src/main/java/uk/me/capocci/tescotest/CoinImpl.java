package uk.me.capocci.tescotest;

import java.util.HashMap;
import java.util.Map;

/**
 * Representing a known coin. This is a superset of the coins an individual machine may accept.
 * @author michael
 *
 */
public class CoinImpl implements Coin, Comparable<Coin> {

	static final Map<Integer, Coin> KNOWN_COINS = new HashMap<>();
	public static final Coin UNKNOWN = new UnknownCoin();
		
	static
	{
		KNOWN_COINS.put(1, new CoinImpl(1));
		KNOWN_COINS.put(2, new CoinImpl(2));
		KNOWN_COINS.put(5, new CoinImpl(5));
		KNOWN_COINS.put(10, new CoinImpl(10));
		KNOWN_COINS.put(20, new CoinImpl(20));
		KNOWN_COINS.put(50, new CoinImpl(50));
		KNOWN_COINS.put(100, new CoinImpl(100));
		KNOWN_COINS.put(200, new CoinImpl(200));
	}
	
	/**
	 * Return a coin for the requested {@code quantity} if one exists, or a coin representing an unknown denomination.
	 * @param quantity
	 * @return the coin requested
	 */
	public static Coin identifyCoin(int denomination)
	{
		return KNOWN_COINS.getOrDefault(denomination, UNKNOWN);
	}
		
	
	private final int denomination;
	
	private CoinImpl()
	{
		super();
		this.denomination = -1;
	}
	
	/**
	 * Create a coin of value {@code quantity}
	 * @param quantity
	 * @throws IllegalArgumentException
	 */
	private CoinImpl(int quantity) throws IllegalArgumentException
	{
		if (quantity <= 0)
			throw new IllegalArgumentException("Coins must have positive quantity");
		this.denomination = quantity;
	}
	

	/**
	 * 
	 * @return quantity embodied by coin
	 */
	public int getDenomination() {
		return denomination;
	}

	@Override
	public int compareTo(Coin o) 
	{
		return Integer.signum(o.getDenomination() - getDenomination());
	}

}
