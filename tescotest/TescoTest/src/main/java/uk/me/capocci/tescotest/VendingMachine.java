package uk.me.capocci.tescotest;

import java.util.List;
import java.util.Optional;

import uk.me.capocci.tescotest.exceptions.InvalidProductException;

public interface VendingMachine {
 
	/**
	 * Visible on machine, as power light and a message LCD containing message and total of coins already inserted!
	 * @return is machine on
	 */
	boolean isOn();
	
	/**
	 * Message currently displayed
	 * @return message
	 */
	String getMessage();
	
	/**
	 * Currently held customer cash
	 * @return cash message
	 */
	String getCustomerCashMessage();
		
	// Actions on the machine
	
	void turnOn();
	void turnOff();
	
	/**
	 * Insert a coin into the machine
	 * @param coin
	 * @return the coin, if it is rejected
	 */
	Optional<Coin> insertCoin(Coin coin);
	
	/**
	 * Return the customer's cash
	 * @return the coins
	 */
	List<Coin> returnCoins();
	
	// 
	/**
	 * Vend the item named {@code name} if possible.
	 * @param name
	 * @return the item named {@code name} if vended
	 * @throws InvalidProductException The buttons are coded to the vending slots, so the exception will be thrown only for a machine mis-configuration
	 */
	Optional<Item> vendItem(String name)  throws InvalidProductException;
	
	/**
	 * Set the float level for an individual coin
	 * @param coin
	 * @param number
	 * @throws IllegalArgumentException
	 */
	void setFloatLevel(Coin coin, int number) throws IllegalArgumentException;
	/**
	 * Set the product level for an individual product
	 * @param name
	 * @param number
	 * @throws IllegalArgumentException
	 */
	void setProductLevel(String name, int number) throws IllegalArgumentException;
	
	/**
	 * An example vending machine configuration
	 * @return the vending machine
	 */
	static VendingMachine exampleVendingMachine()
	{
		VendingMachineImpl machine = new VendingMachineImpl();
		machine.createDefaultStock();
		machine.createDefaultFloat();
		return new VendingMachineSwitch(machine);
	}
	
}
