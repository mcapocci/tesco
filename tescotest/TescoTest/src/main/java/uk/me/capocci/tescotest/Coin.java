package uk.me.capocci.tescotest;

/**
 * Interface for a coin
 * @author michael
 *
 */
public interface Coin {

	int getDenomination();
	
}
