package uk.me.capocci.tescotest.exceptions;

/**
 * Machine configuration exception - e.g. one of the buttons is mapped to an non-existent product
 * @author michael
 *
 */
public class InvalidProductException extends RuntimeException
{
	private static final long serialVersionUID = 1L;

}
