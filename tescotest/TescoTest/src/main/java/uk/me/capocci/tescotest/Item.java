package uk.me.capocci.tescotest;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * A vendable item with price
 * @author michael
 *
 */
public class Item {
	
	private final static Map<String, Item> ITEMS = new HashMap<>();

	static
	{
		ITEMS.put("A", new Item(60, "A"));
		ITEMS.put("B", new Item(100, "B"));
		ITEMS.put("C", new Item(170, "C"));
	}
	
	/**
	 * Return an item of the given name
	 * @param name
	 * @return the item
	 */
	public static Optional<Item> getItem(String name)
	{
		return Optional.ofNullable(ITEMS.getOrDefault(name, null));
	}
	
	private final int price;
	private final String name;
	
	/**
	 * Create an item
	 * @param price
	 * @param name
	 * @throws IllegalArgumentException
	 */
	private Item(int price, String name) throws IllegalArgumentException
	{
		super();
		if (price <= 0)
			throw new IllegalArgumentException("Items must have positive price");
		if (name == null || name.length() == 0)
			throw new IllegalArgumentException("Items must have a name");
		this.price = price;
		this.name = name;
	}

	/**
	 * 
	 * @return the price
	 */
	public int getPrice() {
		return price;
	}
	/**
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	
}
