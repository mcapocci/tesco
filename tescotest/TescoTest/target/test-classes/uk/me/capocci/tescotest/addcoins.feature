Feature: Purchase Item

  @machineOn
  Scenario: The machine is switched on
    Given The vending machine is off
    When I switch on the machine
    Then The vending machine is on

  @coinHandling
  Scenario Outline: Customer inserts coin
    Given The vending machine is on
    When I insert <coin>p
    Then the machine displays <coin>p

    Examples: 
      | coin |
      |   10 |
      |   20 |
      |   50 |
      |  100 |

  Scenario Outline: Customer inserts extra coins
    Given The vending machine is on
    And I insert <coin>p
    When I insert <next>p
    Then the machine displays <total>p

    Examples: 
      | coin | next | total |
      |   10 |   50 |    60 |
      |   20 |  100 |   120 |
      |   50 |   20 |    70 |
      |  100 |  100 |   200 |

  Scenario: Customer requires return of coins
    Given The vending machine is on
    And I insert 50p
    When I request coin return
    Then I receive 50p
    And the machine displays 0p

  Scenario: Customer inserts coin of unaccepted denomination
    Given The vending machine is on
    When I insert 5p
    Then I receive 5p
    And the machine displays 0p

  Scenario: Customer inserts coins of correct and unaccepted denomination
    Given The vending machine is on
    When I insert 10p
    And I insert 5p
    Then I receive 5p
    And the machine displays 10p

  Scenario: Customer inserts unknown coin
    Given The vending machine is on
    When I insert 10c
    Then I receive coin back
    And the machine displays 0p

  @Vending
  Scenario Outline: Buy a product successfully with exact change
    Given The vending machine is on
    And I inserted <coin>p
    And I inserted <coin1>p
    And I inserted <coin2>p
    When I select <product>
    Then product <product> is released
    And the machine displays "Thank you"
    When I request coin return
    Then I receive no change

    Examples: 
      | coin | coin1 | coin2 | product |
      |   10 |    50 |     0 | A       |
      |   50 |    50 |     0 | B       |
      |  100 |    50 |    20 | C       |

  Scenario Outline: Buy a product successfully, with change available
    Given The vending machine is on
    And I inserted <coin>p
    And I inserted <coin1>p
    When I select <product>
    Then product <product> is released
    And the machine displays <change>p
    And the machine displays "Thank you"
    When I request coin return
    Then I receive <change>p

    Examples: 
      | coin | coin1 | product | change |
      |   50 |    20 | A       |     10 |
      |  100 |    20 | B       |     20 |
      |  100 |   100 | C       |     30 |

  Scenario Outline: Fail to buy a product because of insufficient money
    Given The vending machine is on
    And I inserted <coin>p
    When I select <product>
    Then no product is released
    And the machine displays "Insufficient Money Inserted"
    And the machine displays <coin>p
    When I request coin return
    Then I receive <coin>p

    Examples: 
      | coin | product |
      |   20 | A       |
      |   50 | B       |
      |  100 | C       |

  Scenario: Fail to buy a product because of insufficient change in machine
    Given The vending machine is on
    And I inserted 100p
    And Machine has 1 10p coin
    And Machine has 1 20p coin
    When I select A
    Then no product is released
    And the machine displays 100p
    And the machine displays "Exact Change Required"
    When I request coin return
    Then I receive 100p 

  Scenario Outline: Fail to buy a product because of insufficient stock
    Given The vending machine is on
    And product <product> is unavailable
    And I inserted <coin>p
    And I inserted <coin1>p
    When I select <product>
    Then no product is released
    And the machine displays "Item Not Available"
    And the machine displays <total>p
    When I request coin return
    Then I receive <total>p 

    Examples: 
      | coin | coin1 | product | total |
      |   50 |    20 | A       |    70 |
      |  100 |    20 | B       |   120 |
      |  100 |   100 | C       |   200 |
