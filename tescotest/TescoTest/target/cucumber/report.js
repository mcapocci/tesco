$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("addcoins.feature");
formatter.feature({
  "line": 1,
  "name": "Purchase Item",
  "description": "",
  "id": "purchase-item",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 4,
  "name": "The machine is switched on",
  "description": "",
  "id": "purchase-item;the-machine-is-switched-on",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@machineOn"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "The vending machine is off",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I switch on the machine",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "The vending machine is on",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "off",
      "offset": 23
    }
  ],
  "location": "VendingSteps.togglePower(String)"
});
formatter.result({
  "duration": 105129472,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "on",
      "offset": 9
    }
  ],
  "location": "VendingSteps.togglePower(String)"
});
formatter.result({
  "duration": 50944,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "on",
      "offset": 23
    }
  ],
  "location": "VendingSteps.togglePower(String)"
});
formatter.result({
  "duration": 33792,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 10,
  "name": "Customer inserts coin",
  "description": "",
  "id": "purchase-item;customer-inserts-coin",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 9,
      "name": "@coinHandling"
    }
  ]
});
formatter.step({
  "line": 11,
  "name": "The vending machine is on",
  "keyword": "Given "
});
formatter.step({
  "line": 12,
  "name": "I insert \u003ccoin\u003ep",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "the machine displays \u003ccoin\u003ep",
  "keyword": "Then "
});
formatter.examples({
  "line": 15,
  "name": "",
  "description": "",
  "id": "purchase-item;customer-inserts-coin;",
  "rows": [
    {
      "cells": [
        "coin"
      ],
      "line": 16,
      "id": "purchase-item;customer-inserts-coin;;1"
    },
    {
      "cells": [
        "10"
      ],
      "line": 17,
      "id": "purchase-item;customer-inserts-coin;;2"
    },
    {
      "cells": [
        "20"
      ],
      "line": 18,
      "id": "purchase-item;customer-inserts-coin;;3"
    },
    {
      "cells": [
        "50"
      ],
      "line": 19,
      "id": "purchase-item;customer-inserts-coin;;4"
    },
    {
      "cells": [
        "100"
      ],
      "line": 20,
      "id": "purchase-item;customer-inserts-coin;;5"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 17,
  "name": "Customer inserts coin",
  "description": "",
  "id": "purchase-item;customer-inserts-coin;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 9,
      "name": "@coinHandling"
    }
  ]
});
formatter.step({
  "line": 11,
  "name": "The vending machine is on",
  "keyword": "Given "
});
formatter.step({
  "line": 12,
  "name": "I insert 10p",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "the machine displays 10p",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "on",
      "offset": 23
    }
  ],
  "location": "VendingSteps.togglePower(String)"
});
formatter.result({
  "duration": 67072,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "10",
      "offset": 9
    },
    {
      "val": "p",
      "offset": 11
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 1559039,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "10p",
      "offset": 21
    }
  ],
  "location": "VendingSteps.checkMoneyDisplay(String)"
});
formatter.result({
  "duration": 4670721,
  "status": "passed"
});
formatter.scenario({
  "line": 18,
  "name": "Customer inserts coin",
  "description": "",
  "id": "purchase-item;customer-inserts-coin;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 9,
      "name": "@coinHandling"
    }
  ]
});
formatter.step({
  "line": 11,
  "name": "The vending machine is on",
  "keyword": "Given "
});
formatter.step({
  "line": 12,
  "name": "I insert 20p",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "the machine displays 20p",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "on",
      "offset": 23
    }
  ],
  "location": "VendingSteps.togglePower(String)"
});
formatter.result({
  "duration": 62976,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "20",
      "offset": 9
    },
    {
      "val": "p",
      "offset": 11
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 71424,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "20p",
      "offset": 21
    }
  ],
  "location": "VendingSteps.checkMoneyDisplay(String)"
});
formatter.result({
  "duration": 50433,
  "status": "passed"
});
formatter.scenario({
  "line": 19,
  "name": "Customer inserts coin",
  "description": "",
  "id": "purchase-item;customer-inserts-coin;;4",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 9,
      "name": "@coinHandling"
    }
  ]
});
formatter.step({
  "line": 11,
  "name": "The vending machine is on",
  "keyword": "Given "
});
formatter.step({
  "line": 12,
  "name": "I insert 50p",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "the machine displays 50p",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "on",
      "offset": 23
    }
  ],
  "location": "VendingSteps.togglePower(String)"
});
formatter.result({
  "duration": 54528,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "50",
      "offset": 9
    },
    {
      "val": "p",
      "offset": 11
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 60928,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "50p",
      "offset": 21
    }
  ],
  "location": "VendingSteps.checkMoneyDisplay(String)"
});
formatter.result({
  "duration": 45312,
  "status": "passed"
});
formatter.scenario({
  "line": 20,
  "name": "Customer inserts coin",
  "description": "",
  "id": "purchase-item;customer-inserts-coin;;5",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 9,
      "name": "@coinHandling"
    }
  ]
});
formatter.step({
  "line": 11,
  "name": "The vending machine is on",
  "keyword": "Given "
});
formatter.step({
  "line": 12,
  "name": "I insert 100p",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "the machine displays 100p",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "on",
      "offset": 23
    }
  ],
  "location": "VendingSteps.togglePower(String)"
});
formatter.result({
  "duration": 48127,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "100",
      "offset": 9
    },
    {
      "val": "p",
      "offset": 12
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 58367,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "100p",
      "offset": 21
    }
  ],
  "location": "VendingSteps.checkMoneyDisplay(String)"
});
formatter.result({
  "duration": 49409,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 22,
  "name": "Customer inserts extra coins",
  "description": "",
  "id": "purchase-item;customer-inserts-extra-coins",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 23,
  "name": "The vending machine is on",
  "keyword": "Given "
});
formatter.step({
  "line": 24,
  "name": "I insert \u003ccoin\u003ep",
  "keyword": "And "
});
formatter.step({
  "line": 25,
  "name": "I insert \u003cnext\u003ep",
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "the machine displays \u003ctotal\u003ep",
  "keyword": "Then "
});
formatter.examples({
  "line": 28,
  "name": "",
  "description": "",
  "id": "purchase-item;customer-inserts-extra-coins;",
  "rows": [
    {
      "cells": [
        "coin",
        "next",
        "total"
      ],
      "line": 29,
      "id": "purchase-item;customer-inserts-extra-coins;;1"
    },
    {
      "cells": [
        "10",
        "50",
        "60"
      ],
      "line": 30,
      "id": "purchase-item;customer-inserts-extra-coins;;2"
    },
    {
      "cells": [
        "20",
        "100",
        "120"
      ],
      "line": 31,
      "id": "purchase-item;customer-inserts-extra-coins;;3"
    },
    {
      "cells": [
        "50",
        "20",
        "70"
      ],
      "line": 32,
      "id": "purchase-item;customer-inserts-extra-coins;;4"
    },
    {
      "cells": [
        "100",
        "100",
        "200"
      ],
      "line": 33,
      "id": "purchase-item;customer-inserts-extra-coins;;5"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 30,
  "name": "Customer inserts extra coins",
  "description": "",
  "id": "purchase-item;customer-inserts-extra-coins;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 23,
  "name": "The vending machine is on",
  "keyword": "Given "
});
formatter.step({
  "line": 24,
  "name": "I insert 10p",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 25,
  "name": "I insert 50p",
  "matchedColumns": [
    1
  ],
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "the machine displays 60p",
  "matchedColumns": [
    2
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "on",
      "offset": 23
    }
  ],
  "location": "VendingSteps.togglePower(String)"
});
formatter.result({
  "duration": 89088,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "10",
      "offset": 9
    },
    {
      "val": "p",
      "offset": 11
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 70656,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "50",
      "offset": 9
    },
    {
      "val": "p",
      "offset": 11
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 72704,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "60p",
      "offset": 21
    }
  ],
  "location": "VendingSteps.checkMoneyDisplay(String)"
});
formatter.result({
  "duration": 95232,
  "status": "passed"
});
formatter.scenario({
  "line": 31,
  "name": "Customer inserts extra coins",
  "description": "",
  "id": "purchase-item;customer-inserts-extra-coins;;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 23,
  "name": "The vending machine is on",
  "keyword": "Given "
});
formatter.step({
  "line": 24,
  "name": "I insert 20p",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 25,
  "name": "I insert 100p",
  "matchedColumns": [
    1
  ],
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "the machine displays 120p",
  "matchedColumns": [
    2
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "on",
      "offset": 23
    }
  ],
  "location": "VendingSteps.togglePower(String)"
});
formatter.result({
  "duration": 71681,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "20",
      "offset": 9
    },
    {
      "val": "p",
      "offset": 11
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 79617,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "100",
      "offset": 9
    },
    {
      "val": "p",
      "offset": 12
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 88832,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "120p",
      "offset": 21
    }
  ],
  "location": "VendingSteps.checkMoneyDisplay(String)"
});
formatter.result({
  "duration": 62207,
  "status": "passed"
});
formatter.scenario({
  "line": 32,
  "name": "Customer inserts extra coins",
  "description": "",
  "id": "purchase-item;customer-inserts-extra-coins;;4",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 23,
  "name": "The vending machine is on",
  "keyword": "Given "
});
formatter.step({
  "line": 24,
  "name": "I insert 50p",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 25,
  "name": "I insert 20p",
  "matchedColumns": [
    1
  ],
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "the machine displays 70p",
  "matchedColumns": [
    2
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "on",
      "offset": 23
    }
  ],
  "location": "VendingSteps.togglePower(String)"
});
formatter.result({
  "duration": 93441,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "50",
      "offset": 9
    },
    {
      "val": "p",
      "offset": 11
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 101632,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "20",
      "offset": 9
    },
    {
      "val": "p",
      "offset": 11
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 79615,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "70p",
      "offset": 21
    }
  ],
  "location": "VendingSteps.checkMoneyDisplay(String)"
});
formatter.result({
  "duration": 66560,
  "status": "passed"
});
formatter.scenario({
  "line": 33,
  "name": "Customer inserts extra coins",
  "description": "",
  "id": "purchase-item;customer-inserts-extra-coins;;5",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 23,
  "name": "The vending machine is on",
  "keyword": "Given "
});
formatter.step({
  "line": 24,
  "name": "I insert 100p",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 25,
  "name": "I insert 100p",
  "matchedColumns": [
    1
  ],
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "the machine displays 200p",
  "matchedColumns": [
    2
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "on",
      "offset": 23
    }
  ],
  "location": "VendingSteps.togglePower(String)"
});
formatter.result({
  "duration": 76800,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "100",
      "offset": 9
    },
    {
      "val": "p",
      "offset": 12
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 89856,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "100",
      "offset": 9
    },
    {
      "val": "p",
      "offset": 12
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 75009,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200p",
      "offset": 21
    }
  ],
  "location": "VendingSteps.checkMoneyDisplay(String)"
});
formatter.result({
  "duration": 67840,
  "status": "passed"
});
formatter.scenario({
  "line": 35,
  "name": "Customer requires return of coins",
  "description": "",
  "id": "purchase-item;customer-requires-return-of-coins",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 36,
  "name": "The vending machine is on",
  "keyword": "Given "
});
formatter.step({
  "line": 37,
  "name": "I insert 50p",
  "keyword": "And "
});
formatter.step({
  "line": 38,
  "name": "I request coin return",
  "keyword": "When "
});
formatter.step({
  "line": 39,
  "name": "I receive 50p",
  "keyword": "Then "
});
formatter.step({
  "line": 40,
  "name": "the machine displays 0p",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "on",
      "offset": 23
    }
  ],
  "location": "VendingSteps.togglePower(String)"
});
formatter.result({
  "duration": 67584,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "50",
      "offset": 9
    },
    {
      "val": "p",
      "offset": 11
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 76288,
  "status": "passed"
});
formatter.match({
  "location": "VendingSteps.coinReturn()"
});
formatter.result({
  "duration": 37119,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "50",
      "offset": 10
    }
  ],
  "location": "VendingSteps.receiveCoins(int)"
});
formatter.result({
  "duration": 482304,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "0p",
      "offset": 21
    }
  ],
  "location": "VendingSteps.checkMoneyDisplay(String)"
});
formatter.result({
  "duration": 54016,
  "status": "passed"
});
formatter.scenario({
  "line": 42,
  "name": "Customer inserts coin of unaccepted denomination",
  "description": "",
  "id": "purchase-item;customer-inserts-coin-of-unaccepted-denomination",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 43,
  "name": "The vending machine is on",
  "keyword": "Given "
});
formatter.step({
  "line": 44,
  "name": "I insert 5p",
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "I receive 5p",
  "keyword": "Then "
});
formatter.step({
  "line": 46,
  "name": "the machine displays 0p",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "on",
      "offset": 23
    }
  ],
  "location": "VendingSteps.togglePower(String)"
});
formatter.result({
  "duration": 69120,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "5",
      "offset": 9
    },
    {
      "val": "p",
      "offset": 10
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 86785,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "5",
      "offset": 10
    }
  ],
  "location": "VendingSteps.receiveCoins(int)"
});
formatter.result({
  "duration": 217343,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "0p",
      "offset": 21
    }
  ],
  "location": "VendingSteps.checkMoneyDisplay(String)"
});
formatter.result({
  "duration": 58881,
  "status": "passed"
});
formatter.scenario({
  "line": 48,
  "name": "Customer inserts coins of correct and unaccepted denomination",
  "description": "",
  "id": "purchase-item;customer-inserts-coins-of-correct-and-unaccepted-denomination",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 49,
  "name": "The vending machine is on",
  "keyword": "Given "
});
formatter.step({
  "line": 50,
  "name": "I insert 10p",
  "keyword": "When "
});
formatter.step({
  "line": 51,
  "name": "I insert 5p",
  "keyword": "And "
});
formatter.step({
  "line": 52,
  "name": "I receive 5p",
  "keyword": "Then "
});
formatter.step({
  "line": 53,
  "name": "the machine displays 10p",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "on",
      "offset": 23
    }
  ],
  "location": "VendingSteps.togglePower(String)"
});
formatter.result({
  "duration": 70143,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "10",
      "offset": 9
    },
    {
      "val": "p",
      "offset": 11
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 83200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "5",
      "offset": 9
    },
    {
      "val": "p",
      "offset": 10
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 413695,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "5",
      "offset": 10
    }
  ],
  "location": "VendingSteps.receiveCoins(int)"
});
formatter.result({
  "duration": 72448,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "10p",
      "offset": 21
    }
  ],
  "location": "VendingSteps.checkMoneyDisplay(String)"
});
formatter.result({
  "duration": 50176,
  "status": "passed"
});
formatter.scenario({
  "line": 55,
  "name": "Customer inserts unknown coin",
  "description": "",
  "id": "purchase-item;customer-inserts-unknown-coin",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 56,
  "name": "The vending machine is on",
  "keyword": "Given "
});
formatter.step({
  "line": 57,
  "name": "I insert 10c",
  "keyword": "When "
});
formatter.step({
  "line": 58,
  "name": "I receive coin back",
  "keyword": "Then "
});
formatter.step({
  "line": 59,
  "name": "the machine displays 0p",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "on",
      "offset": 23
    }
  ],
  "location": "VendingSteps.togglePower(String)"
});
formatter.result({
  "duration": 83711,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "10",
      "offset": 9
    },
    {
      "val": "c",
      "offset": 11
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 98304,
  "status": "passed"
});
formatter.match({
  "location": "VendingSteps.receiveCoinBack()"
});
formatter.result({
  "duration": 41472,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "0p",
      "offset": 21
    }
  ],
  "location": "VendingSteps.checkMoneyDisplay(String)"
});
formatter.result({
  "duration": 69376,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 62,
  "name": "Buy a product successfully with exact change",
  "description": "",
  "id": "purchase-item;buy-a-product-successfully-with-exact-change",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 61,
      "name": "@Vending"
    }
  ]
});
formatter.step({
  "line": 63,
  "name": "The vending machine is on",
  "keyword": "Given "
});
formatter.step({
  "line": 64,
  "name": "I inserted \u003ccoin\u003ep",
  "keyword": "And "
});
formatter.step({
  "line": 65,
  "name": "I inserted \u003ccoin1\u003ep",
  "keyword": "And "
});
formatter.step({
  "line": 66,
  "name": "I inserted \u003ccoin2\u003ep",
  "keyword": "And "
});
formatter.step({
  "line": 67,
  "name": "I select A",
  "keyword": "When "
});
formatter.step({
  "line": 68,
  "name": "product A is released",
  "keyword": "Then "
});
formatter.step({
  "line": 69,
  "name": "I receive no change",
  "keyword": "And "
});
formatter.step({
  "line": 70,
  "name": "the machine displays \"Thank you\"",
  "keyword": "And "
});
formatter.examples({
  "line": 72,
  "name": "",
  "description": "",
  "id": "purchase-item;buy-a-product-successfully-with-exact-change;",
  "rows": [
    {
      "cells": [
        "coin",
        "coin1",
        "coin2",
        "product"
      ],
      "line": 73,
      "id": "purchase-item;buy-a-product-successfully-with-exact-change;;1"
    },
    {
      "cells": [
        "10",
        "50",
        "0",
        "A"
      ],
      "line": 74,
      "id": "purchase-item;buy-a-product-successfully-with-exact-change;;2"
    },
    {
      "cells": [
        "50",
        "50",
        "0",
        "B"
      ],
      "line": 75,
      "id": "purchase-item;buy-a-product-successfully-with-exact-change;;3"
    },
    {
      "cells": [
        "100",
        "20",
        "20",
        "C"
      ],
      "line": 76,
      "id": "purchase-item;buy-a-product-successfully-with-exact-change;;4"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 74,
  "name": "Buy a product successfully with exact change",
  "description": "",
  "id": "purchase-item;buy-a-product-successfully-with-exact-change;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 61,
      "name": "@Vending"
    }
  ]
});
formatter.step({
  "line": 63,
  "name": "The vending machine is on",
  "keyword": "Given "
});
formatter.step({
  "line": 64,
  "name": "I inserted 10p",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 65,
  "name": "I inserted 50p",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 66,
  "name": "I inserted 0p",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 67,
  "name": "I select A",
  "keyword": "When "
});
formatter.step({
  "line": 68,
  "name": "product A is released",
  "keyword": "Then "
});
formatter.step({
  "line": 69,
  "name": "I receive no change",
  "keyword": "And "
});
formatter.step({
  "line": 70,
  "name": "the machine displays \"Thank you\"",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "on",
      "offset": 23
    }
  ],
  "location": "VendingSteps.togglePower(String)"
});
formatter.result({
  "duration": 347137,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "10",
      "offset": 11
    },
    {
      "val": "p",
      "offset": 13
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 89343,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "50",
      "offset": 11
    },
    {
      "val": "p",
      "offset": 13
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 75520,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "0",
      "offset": 11
    },
    {
      "val": "p",
      "offset": 12
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 55040,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "A",
      "offset": 9
    }
  ],
  "location": "VendingSteps.selectProduct(String)"
});
formatter.result({
  "duration": 687616,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "A",
      "offset": 8
    }
  ],
  "location": "VendingSteps.productReceived(String)"
});
formatter.result({
  "duration": 50944,
  "status": "passed"
});
formatter.match({
  "location": "VendingSteps.receiveNoCoins()"
});
formatter.result({
  "duration": 15872,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Thank you",
      "offset": 22
    }
  ],
  "location": "VendingSteps.checkDisplay(String)"
});
formatter.result({
  "duration": 47104,
  "status": "passed"
});
formatter.scenario({
  "line": 75,
  "name": "Buy a product successfully with exact change",
  "description": "",
  "id": "purchase-item;buy-a-product-successfully-with-exact-change;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 61,
      "name": "@Vending"
    }
  ]
});
formatter.step({
  "line": 63,
  "name": "The vending machine is on",
  "keyword": "Given "
});
formatter.step({
  "line": 64,
  "name": "I inserted 50p",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 65,
  "name": "I inserted 50p",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 66,
  "name": "I inserted 0p",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 67,
  "name": "I select A",
  "keyword": "When "
});
formatter.step({
  "line": 68,
  "name": "product A is released",
  "keyword": "Then "
});
formatter.step({
  "line": 69,
  "name": "I receive no change",
  "keyword": "And "
});
formatter.step({
  "line": 70,
  "name": "the machine displays \"Thank you\"",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "on",
      "offset": 23
    }
  ],
  "location": "VendingSteps.togglePower(String)"
});
formatter.result({
  "duration": 70144,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "50",
      "offset": 11
    },
    {
      "val": "p",
      "offset": 13
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 56832,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "50",
      "offset": 11
    },
    {
      "val": "p",
      "offset": 13
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 50175,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "0",
      "offset": 11
    },
    {
      "val": "p",
      "offset": 12
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 32512,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "A",
      "offset": 9
    }
  ],
  "location": "VendingSteps.selectProduct(String)"
});
formatter.result({
  "duration": 4042496,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "A",
      "offset": 8
    }
  ],
  "location": "VendingSteps.productReceived(String)"
});
formatter.result({
  "duration": 38912,
  "status": "passed"
});
formatter.match({
  "location": "VendingSteps.receiveNoCoins()"
});
formatter.result({
  "duration": 10240,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Thank you",
      "offset": 22
    }
  ],
  "location": "VendingSteps.checkDisplay(String)"
});
formatter.result({
  "duration": 32255,
  "status": "passed"
});
formatter.scenario({
  "line": 76,
  "name": "Buy a product successfully with exact change",
  "description": "",
  "id": "purchase-item;buy-a-product-successfully-with-exact-change;;4",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 61,
      "name": "@Vending"
    }
  ]
});
formatter.step({
  "line": 63,
  "name": "The vending machine is on",
  "keyword": "Given "
});
formatter.step({
  "line": 64,
  "name": "I inserted 100p",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 65,
  "name": "I inserted 20p",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 66,
  "name": "I inserted 20p",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 67,
  "name": "I select A",
  "keyword": "When "
});
formatter.step({
  "line": 68,
  "name": "product A is released",
  "keyword": "Then "
});
formatter.step({
  "line": 69,
  "name": "I receive no change",
  "keyword": "And "
});
formatter.step({
  "line": 70,
  "name": "the machine displays \"Thank you\"",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "on",
      "offset": 23
    }
  ],
  "location": "VendingSteps.togglePower(String)"
});
formatter.result({
  "duration": 385792,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "100",
      "offset": 11
    },
    {
      "val": "p",
      "offset": 14
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 86015,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "20",
      "offset": 11
    },
    {
      "val": "p",
      "offset": 13
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 78848,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "20",
      "offset": 11
    },
    {
      "val": "p",
      "offset": 13
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 65280,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "A",
      "offset": 9
    }
  ],
  "location": "VendingSteps.selectProduct(String)"
});
formatter.result({
  "duration": 169472,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "A",
      "offset": 8
    }
  ],
  "location": "VendingSteps.productReceived(String)"
});
formatter.result({
  "duration": 23808,
  "status": "passed"
});
formatter.match({
  "location": "VendingSteps.receiveNoCoins()"
});
formatter.result({
  "duration": 8703,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Thank you",
      "offset": 22
    }
  ],
  "location": "VendingSteps.checkDisplay(String)"
});
formatter.result({
  "duration": 24832,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 78,
  "name": "Buy a product successfully, with change available",
  "description": "",
  "id": "purchase-item;buy-a-product-successfully,-with-change-available",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 79,
  "name": "The vending machine is on",
  "keyword": "Given "
});
formatter.step({
  "line": 80,
  "name": "I inserted \u003ccoin\u003ep",
  "keyword": "And "
});
formatter.step({
  "line": 81,
  "name": "I inserted \u003ccoin1\u003ep",
  "keyword": "And "
});
formatter.step({
  "line": 82,
  "name": "I select \u003cproduct\u003e",
  "keyword": "When "
});
formatter.step({
  "line": 83,
  "name": "product \u003cproduct\u003e is released",
  "keyword": "Then "
});
formatter.step({
  "line": 84,
  "name": "the machine displays \u003cchange\u003ep",
  "keyword": "And "
});
formatter.step({
  "line": 85,
  "name": "the machine displays \"Thank you\"",
  "keyword": "And "
});
formatter.examples({
  "line": 87,
  "name": "",
  "description": "",
  "id": "purchase-item;buy-a-product-successfully,-with-change-available;",
  "rows": [
    {
      "cells": [
        "coin",
        "coin1",
        "product",
        "change"
      ],
      "line": 88,
      "id": "purchase-item;buy-a-product-successfully,-with-change-available;;1"
    },
    {
      "cells": [
        "50",
        "20",
        "A",
        "10"
      ],
      "line": 89,
      "id": "purchase-item;buy-a-product-successfully,-with-change-available;;2"
    },
    {
      "cells": [
        "100",
        "20",
        "B",
        "20"
      ],
      "line": 90,
      "id": "purchase-item;buy-a-product-successfully,-with-change-available;;3"
    },
    {
      "cells": [
        "100",
        "100",
        "C",
        "30"
      ],
      "line": 91,
      "id": "purchase-item;buy-a-product-successfully,-with-change-available;;4"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 89,
  "name": "Buy a product successfully, with change available",
  "description": "",
  "id": "purchase-item;buy-a-product-successfully,-with-change-available;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 79,
  "name": "The vending machine is on",
  "keyword": "Given "
});
formatter.step({
  "line": 80,
  "name": "I inserted 50p",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 81,
  "name": "I inserted 20p",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 82,
  "name": "I select A",
  "matchedColumns": [
    2
  ],
  "keyword": "When "
});
formatter.step({
  "line": 83,
  "name": "product A is released",
  "matchedColumns": [
    2
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 84,
  "name": "the machine displays 10p",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.step({
  "line": 85,
  "name": "the machine displays \"Thank you\"",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "on",
      "offset": 23
    }
  ],
  "location": "VendingSteps.togglePower(String)"
});
formatter.result({
  "duration": 85248,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "50",
      "offset": 11
    },
    {
      "val": "p",
      "offset": 13
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 95744,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "20",
      "offset": 11
    },
    {
      "val": "p",
      "offset": 13
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 70144,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "A",
      "offset": 9
    }
  ],
  "location": "VendingSteps.selectProduct(String)"
});
formatter.result({
  "duration": 123136,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "A",
      "offset": 8
    }
  ],
  "location": "VendingSteps.productReceived(String)"
});
formatter.result({
  "duration": 31488,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "10p",
      "offset": 21
    }
  ],
  "location": "VendingSteps.checkMoneyDisplay(String)"
});
formatter.result({
  "duration": 53503,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Thank you",
      "offset": 22
    }
  ],
  "location": "VendingSteps.checkDisplay(String)"
});
formatter.result({
  "duration": 29184,
  "status": "passed"
});
formatter.scenario({
  "line": 90,
  "name": "Buy a product successfully, with change available",
  "description": "",
  "id": "purchase-item;buy-a-product-successfully,-with-change-available;;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 79,
  "name": "The vending machine is on",
  "keyword": "Given "
});
formatter.step({
  "line": 80,
  "name": "I inserted 100p",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 81,
  "name": "I inserted 20p",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 82,
  "name": "I select B",
  "matchedColumns": [
    2
  ],
  "keyword": "When "
});
formatter.step({
  "line": 83,
  "name": "product B is released",
  "matchedColumns": [
    2
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 84,
  "name": "the machine displays 20p",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.step({
  "line": 85,
  "name": "the machine displays \"Thank you\"",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "on",
      "offset": 23
    }
  ],
  "location": "VendingSteps.togglePower(String)"
});
formatter.result({
  "duration": 58881,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "100",
      "offset": 11
    },
    {
      "val": "p",
      "offset": 14
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 73984,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "20",
      "offset": 11
    },
    {
      "val": "p",
      "offset": 13
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 67071,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "B",
      "offset": 9
    }
  ],
  "location": "VendingSteps.selectProduct(String)"
});
formatter.result({
  "duration": 97281,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "B",
      "offset": 8
    }
  ],
  "location": "VendingSteps.productReceived(String)"
});
formatter.result({
  "duration": 28928,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "20p",
      "offset": 21
    }
  ],
  "location": "VendingSteps.checkMoneyDisplay(String)"
});
formatter.result({
  "duration": 48129,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Thank you",
      "offset": 22
    }
  ],
  "location": "VendingSteps.checkDisplay(String)"
});
formatter.result({
  "duration": 20992,
  "status": "passed"
});
formatter.scenario({
  "line": 91,
  "name": "Buy a product successfully, with change available",
  "description": "",
  "id": "purchase-item;buy-a-product-successfully,-with-change-available;;4",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 79,
  "name": "The vending machine is on",
  "keyword": "Given "
});
formatter.step({
  "line": 80,
  "name": "I inserted 100p",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 81,
  "name": "I inserted 100p",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 82,
  "name": "I select C",
  "matchedColumns": [
    2
  ],
  "keyword": "When "
});
formatter.step({
  "line": 83,
  "name": "product C is released",
  "matchedColumns": [
    2
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 84,
  "name": "the machine displays 30p",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.step({
  "line": 85,
  "name": "the machine displays \"Thank you\"",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "on",
      "offset": 23
    }
  ],
  "location": "VendingSteps.togglePower(String)"
});
formatter.result({
  "duration": 38912,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "100",
      "offset": 11
    },
    {
      "val": "p",
      "offset": 14
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 50432,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "100",
      "offset": 11
    },
    {
      "val": "p",
      "offset": 14
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 43776,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "C",
      "offset": 9
    }
  ],
  "location": "VendingSteps.selectProduct(String)"
});
formatter.result({
  "duration": 99840,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "C",
      "offset": 8
    }
  ],
  "location": "VendingSteps.productReceived(String)"
});
formatter.result({
  "duration": 20224,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "30p",
      "offset": 21
    }
  ],
  "location": "VendingSteps.checkMoneyDisplay(String)"
});
formatter.result({
  "duration": 36863,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Thank you",
      "offset": 22
    }
  ],
  "location": "VendingSteps.checkDisplay(String)"
});
formatter.result({
  "duration": 18943,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 93,
  "name": "Fail to buy a product because of insufficient money",
  "description": "",
  "id": "purchase-item;fail-to-buy-a-product-because-of-insufficient-money",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 94,
  "name": "The vending machine is on",
  "keyword": "Given "
});
formatter.step({
  "line": 95,
  "name": "I inserted \u003ccoin\u003ep",
  "keyword": "And "
});
formatter.step({
  "line": 96,
  "name": "I select \u003cproduct\u003e",
  "keyword": "When "
});
formatter.step({
  "line": 97,
  "name": "no product is released",
  "keyword": "Then "
});
formatter.step({
  "line": 98,
  "name": "the machine displays \"Insufficient Money Inserted\"",
  "keyword": "And "
});
formatter.step({
  "line": 99,
  "name": "the machine displays \u003ccoin\u003ep",
  "keyword": "And "
});
formatter.examples({
  "line": 101,
  "name": "",
  "description": "",
  "id": "purchase-item;fail-to-buy-a-product-because-of-insufficient-money;",
  "rows": [
    {
      "cells": [
        "coin",
        "product"
      ],
      "line": 102,
      "id": "purchase-item;fail-to-buy-a-product-because-of-insufficient-money;;1"
    },
    {
      "cells": [
        "20",
        "A"
      ],
      "line": 103,
      "id": "purchase-item;fail-to-buy-a-product-because-of-insufficient-money;;2"
    },
    {
      "cells": [
        "50",
        "B"
      ],
      "line": 104,
      "id": "purchase-item;fail-to-buy-a-product-because-of-insufficient-money;;3"
    },
    {
      "cells": [
        "100",
        "C"
      ],
      "line": 105,
      "id": "purchase-item;fail-to-buy-a-product-because-of-insufficient-money;;4"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 103,
  "name": "Fail to buy a product because of insufficient money",
  "description": "",
  "id": "purchase-item;fail-to-buy-a-product-because-of-insufficient-money;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 94,
  "name": "The vending machine is on",
  "keyword": "Given "
});
formatter.step({
  "line": 95,
  "name": "I inserted 20p",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 96,
  "name": "I select A",
  "matchedColumns": [
    1
  ],
  "keyword": "When "
});
formatter.step({
  "line": 97,
  "name": "no product is released",
  "keyword": "Then "
});
formatter.step({
  "line": 98,
  "name": "the machine displays \"Insufficient Money Inserted\"",
  "keyword": "And "
});
formatter.step({
  "line": 99,
  "name": "the machine displays 20p",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "on",
      "offset": 23
    }
  ],
  "location": "VendingSteps.togglePower(String)"
});
formatter.result({
  "duration": 59904,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "20",
      "offset": 11
    },
    {
      "val": "p",
      "offset": 13
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 70655,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "A",
      "offset": 9
    }
  ],
  "location": "VendingSteps.selectProduct(String)"
});
formatter.result({
  "duration": 45824,
  "status": "passed"
});
formatter.match({
  "location": "VendingSteps.productNotReceived()"
});
formatter.result({
  "duration": 17663,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Insufficient Money Inserted",
      "offset": 22
    }
  ],
  "location": "VendingSteps.checkDisplay(String)"
});
formatter.result({
  "duration": 22528,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "20p",
      "offset": 21
    }
  ],
  "location": "VendingSteps.checkMoneyDisplay(String)"
});
formatter.result({
  "duration": 239616,
  "status": "passed"
});
formatter.scenario({
  "line": 104,
  "name": "Fail to buy a product because of insufficient money",
  "description": "",
  "id": "purchase-item;fail-to-buy-a-product-because-of-insufficient-money;;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 94,
  "name": "The vending machine is on",
  "keyword": "Given "
});
formatter.step({
  "line": 95,
  "name": "I inserted 50p",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 96,
  "name": "I select B",
  "matchedColumns": [
    1
  ],
  "keyword": "When "
});
formatter.step({
  "line": 97,
  "name": "no product is released",
  "keyword": "Then "
});
formatter.step({
  "line": 98,
  "name": "the machine displays \"Insufficient Money Inserted\"",
  "keyword": "And "
});
formatter.step({
  "line": 99,
  "name": "the machine displays 50p",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "on",
      "offset": 23
    }
  ],
  "location": "VendingSteps.togglePower(String)"
});
formatter.result({
  "duration": 50945,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "50",
      "offset": 11
    },
    {
      "val": "p",
      "offset": 13
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 48384,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "B",
      "offset": 9
    }
  ],
  "location": "VendingSteps.selectProduct(String)"
});
formatter.result({
  "duration": 36095,
  "status": "passed"
});
formatter.match({
  "location": "VendingSteps.productNotReceived()"
});
formatter.result({
  "duration": 7936,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Insufficient Money Inserted",
      "offset": 22
    }
  ],
  "location": "VendingSteps.checkDisplay(String)"
});
formatter.result({
  "duration": 19968,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "50p",
      "offset": 21
    }
  ],
  "location": "VendingSteps.checkMoneyDisplay(String)"
});
formatter.result({
  "duration": 41728,
  "status": "passed"
});
formatter.scenario({
  "line": 105,
  "name": "Fail to buy a product because of insufficient money",
  "description": "",
  "id": "purchase-item;fail-to-buy-a-product-because-of-insufficient-money;;4",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 94,
  "name": "The vending machine is on",
  "keyword": "Given "
});
formatter.step({
  "line": 95,
  "name": "I inserted 100p",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 96,
  "name": "I select C",
  "matchedColumns": [
    1
  ],
  "keyword": "When "
});
formatter.step({
  "line": 97,
  "name": "no product is released",
  "keyword": "Then "
});
formatter.step({
  "line": 98,
  "name": "the machine displays \"Insufficient Money Inserted\"",
  "keyword": "And "
});
formatter.step({
  "line": 99,
  "name": "the machine displays 100p",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "on",
      "offset": 23
    }
  ],
  "location": "VendingSteps.togglePower(String)"
});
formatter.result({
  "duration": 57600,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "100",
      "offset": 11
    },
    {
      "val": "p",
      "offset": 14
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 56832,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "C",
      "offset": 9
    }
  ],
  "location": "VendingSteps.selectProduct(String)"
});
formatter.result({
  "duration": 61695,
  "status": "passed"
});
formatter.match({
  "location": "VendingSteps.productNotReceived()"
});
formatter.result({
  "duration": 12288,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Insufficient Money Inserted",
      "offset": 22
    }
  ],
  "location": "VendingSteps.checkDisplay(String)"
});
formatter.result({
  "duration": 30976,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "100p",
      "offset": 21
    }
  ],
  "location": "VendingSteps.checkMoneyDisplay(String)"
});
formatter.result({
  "duration": 46336,
  "status": "passed"
});
formatter.scenario({
  "line": 107,
  "name": "Fail to buy a product because of insufficient change in machine",
  "description": "",
  "id": "purchase-item;fail-to-buy-a-product-because-of-insufficient-change-in-machine",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 108,
  "name": "The vending machine is on",
  "keyword": "Given "
});
formatter.step({
  "line": 109,
  "name": "I inserted 100p",
  "keyword": "And "
});
formatter.step({
  "line": 110,
  "name": "Machine has 1 10p coin",
  "keyword": "And "
});
formatter.step({
  "line": 111,
  "name": "Machine has 1 20p coin",
  "keyword": "And "
});
formatter.step({
  "line": 112,
  "name": "I select A",
  "keyword": "When "
});
formatter.step({
  "line": 113,
  "name": "no product is released",
  "keyword": "Then "
});
formatter.step({
  "line": 114,
  "name": "the machine displays 100p",
  "keyword": "And "
});
formatter.step({
  "line": 115,
  "name": "the machine displays \"Exact Change Required\"",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "on",
      "offset": 23
    }
  ],
  "location": "VendingSteps.togglePower(String)"
});
formatter.result({
  "duration": 62464,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "100",
      "offset": 11
    },
    {
      "val": "p",
      "offset": 14
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 73471,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 12
    },
    {
      "val": "10",
      "offset": 14
    }
  ],
  "location": "VendingSteps.setAvailableCoins(int,int)"
});
formatter.result({
  "duration": 81919,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 12
    },
    {
      "val": "20",
      "offset": 14
    }
  ],
  "location": "VendingSteps.setAvailableCoins(int,int)"
});
formatter.result({
  "duration": 69633,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "A",
      "offset": 9
    }
  ],
  "location": "VendingSteps.selectProduct(String)"
});
formatter.result({
  "duration": 161536,
  "status": "passed"
});
formatter.match({
  "location": "VendingSteps.productNotReceived()"
});
formatter.result({
  "duration": 9216,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "100p",
      "offset": 21
    }
  ],
  "location": "VendingSteps.checkMoneyDisplay(String)"
});
formatter.result({
  "duration": 36097,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Exact Change Required",
      "offset": 22
    }
  ],
  "location": "VendingSteps.checkDisplay(String)"
});
formatter.result({
  "duration": 28416,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 117,
  "name": "Fail to buy a product because of insufficient stock",
  "description": "",
  "id": "purchase-item;fail-to-buy-a-product-because-of-insufficient-stock",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 118,
  "name": "The vending machine is on",
  "keyword": "Given "
});
formatter.step({
  "line": 119,
  "name": "product \u003cproduct\u003e is unavailable",
  "keyword": "And "
});
formatter.step({
  "line": 120,
  "name": "I inserted \u003ccoin\u003ep",
  "keyword": "And "
});
formatter.step({
  "line": 121,
  "name": "I inserted \u003ccoin1\u003ep",
  "keyword": "And "
});
formatter.step({
  "line": 122,
  "name": "I select \u003cproduct\u003e",
  "keyword": "When "
});
formatter.step({
  "line": 123,
  "name": "no product is released",
  "keyword": "Then "
});
formatter.step({
  "line": 124,
  "name": "the machine displays \"Item Not Available\"",
  "keyword": "And "
});
formatter.step({
  "line": 125,
  "name": "the machine displays \u003ctotal\u003ep",
  "keyword": "And "
});
formatter.examples({
  "line": 127,
  "name": "",
  "description": "",
  "id": "purchase-item;fail-to-buy-a-product-because-of-insufficient-stock;",
  "rows": [
    {
      "cells": [
        "coin",
        "coin1",
        "product",
        "total"
      ],
      "line": 128,
      "id": "purchase-item;fail-to-buy-a-product-because-of-insufficient-stock;;1"
    },
    {
      "cells": [
        "50",
        "20",
        "A",
        "70"
      ],
      "line": 129,
      "id": "purchase-item;fail-to-buy-a-product-because-of-insufficient-stock;;2"
    },
    {
      "cells": [
        "100",
        "20",
        "B",
        "120"
      ],
      "line": 130,
      "id": "purchase-item;fail-to-buy-a-product-because-of-insufficient-stock;;3"
    },
    {
      "cells": [
        "100",
        "100",
        "C",
        "200"
      ],
      "line": 131,
      "id": "purchase-item;fail-to-buy-a-product-because-of-insufficient-stock;;4"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 129,
  "name": "Fail to buy a product because of insufficient stock",
  "description": "",
  "id": "purchase-item;fail-to-buy-a-product-because-of-insufficient-stock;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 118,
  "name": "The vending machine is on",
  "keyword": "Given "
});
formatter.step({
  "line": 119,
  "name": "product A is unavailable",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 120,
  "name": "I inserted 50p",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 121,
  "name": "I inserted 20p",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 122,
  "name": "I select A",
  "matchedColumns": [
    2
  ],
  "keyword": "When "
});
formatter.step({
  "line": 123,
  "name": "no product is released",
  "keyword": "Then "
});
formatter.step({
  "line": 124,
  "name": "the machine displays \"Item Not Available\"",
  "keyword": "And "
});
formatter.step({
  "line": 125,
  "name": "the machine displays 70p",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "on",
      "offset": 23
    }
  ],
  "location": "VendingSteps.togglePower(String)"
});
formatter.result({
  "duration": 73983,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "A",
      "offset": 8
    }
  ],
  "location": "VendingSteps.setProductUnavailable(String)"
});
formatter.result({
  "duration": 429056,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "50",
      "offset": 11
    },
    {
      "val": "p",
      "offset": 13
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 76800,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "20",
      "offset": 11
    },
    {
      "val": "p",
      "offset": 13
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 50944,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "A",
      "offset": 9
    }
  ],
  "location": "VendingSteps.selectProduct(String)"
});
formatter.result({
  "duration": 32768,
  "status": "passed"
});
formatter.match({
  "location": "VendingSteps.productNotReceived()"
});
formatter.result({
  "duration": 11264,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Item Not Available",
      "offset": 22
    }
  ],
  "location": "VendingSteps.checkDisplay(String)"
});
formatter.result({
  "duration": 33280,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "70p",
      "offset": 21
    }
  ],
  "location": "VendingSteps.checkMoneyDisplay(String)"
});
formatter.result({
  "duration": 64769,
  "status": "passed"
});
formatter.scenario({
  "line": 130,
  "name": "Fail to buy a product because of insufficient stock",
  "description": "",
  "id": "purchase-item;fail-to-buy-a-product-because-of-insufficient-stock;;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 118,
  "name": "The vending machine is on",
  "keyword": "Given "
});
formatter.step({
  "line": 119,
  "name": "product B is unavailable",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 120,
  "name": "I inserted 100p",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 121,
  "name": "I inserted 20p",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 122,
  "name": "I select B",
  "matchedColumns": [
    2
  ],
  "keyword": "When "
});
formatter.step({
  "line": 123,
  "name": "no product is released",
  "keyword": "Then "
});
formatter.step({
  "line": 124,
  "name": "the machine displays \"Item Not Available\"",
  "keyword": "And "
});
formatter.step({
  "line": 125,
  "name": "the machine displays 120p",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "on",
      "offset": 23
    }
  ],
  "location": "VendingSteps.togglePower(String)"
});
formatter.result({
  "duration": 78593,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "B",
      "offset": 8
    }
  ],
  "location": "VendingSteps.setProductUnavailable(String)"
});
formatter.result({
  "duration": 41728,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "100",
      "offset": 11
    },
    {
      "val": "p",
      "offset": 14
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 89088,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "20",
      "offset": 11
    },
    {
      "val": "p",
      "offset": 13
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 71937,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "B",
      "offset": 9
    }
  ],
  "location": "VendingSteps.selectProduct(String)"
});
formatter.result({
  "duration": 35327,
  "status": "passed"
});
formatter.match({
  "location": "VendingSteps.productNotReceived()"
});
formatter.result({
  "duration": 10752,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Item Not Available",
      "offset": 22
    }
  ],
  "location": "VendingSteps.checkDisplay(String)"
});
formatter.result({
  "duration": 29953,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "120p",
      "offset": 21
    }
  ],
  "location": "VendingSteps.checkMoneyDisplay(String)"
});
formatter.result({
  "duration": 56063,
  "status": "passed"
});
formatter.scenario({
  "line": 131,
  "name": "Fail to buy a product because of insufficient stock",
  "description": "",
  "id": "purchase-item;fail-to-buy-a-product-because-of-insufficient-stock;;4",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 118,
  "name": "The vending machine is on",
  "keyword": "Given "
});
formatter.step({
  "line": 119,
  "name": "product C is unavailable",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 120,
  "name": "I inserted 100p",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 121,
  "name": "I inserted 100p",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 122,
  "name": "I select C",
  "matchedColumns": [
    2
  ],
  "keyword": "When "
});
formatter.step({
  "line": 123,
  "name": "no product is released",
  "keyword": "Then "
});
formatter.step({
  "line": 124,
  "name": "the machine displays \"Item Not Available\"",
  "keyword": "And "
});
formatter.step({
  "line": 125,
  "name": "the machine displays 200p",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "on",
      "offset": 23
    }
  ],
  "location": "VendingSteps.togglePower(String)"
});
formatter.result({
  "duration": 66048,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "C",
      "offset": 8
    }
  ],
  "location": "VendingSteps.setProductUnavailable(String)"
});
formatter.result({
  "duration": 34560,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "100",
      "offset": 11
    },
    {
      "val": "p",
      "offset": 14
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 79873,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "100",
      "offset": 11
    },
    {
      "val": "p",
      "offset": 14
    }
  ],
  "location": "VendingSteps.addCoin(int,char)"
});
formatter.result({
  "duration": 46848,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "C",
      "offset": 9
    }
  ],
  "location": "VendingSteps.selectProduct(String)"
});
formatter.result({
  "duration": 34561,
  "status": "passed"
});
formatter.match({
  "location": "VendingSteps.productNotReceived()"
});
formatter.result({
  "duration": 13312,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Item Not Available",
      "offset": 22
    }
  ],
  "location": "VendingSteps.checkDisplay(String)"
});
formatter.result({
  "duration": 52992,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200p",
      "offset": 21
    }
  ],
  "location": "VendingSteps.checkMoneyDisplay(String)"
});
formatter.result({
  "duration": 65024,
  "status": "passed"
});
});